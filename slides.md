---
header-includes:
    - '\usetheme[titleformat=smallcaps,numbering=none,progressbar=frametitle]{metropolis}'
    - '\usepackage[fixed]{fontawesome5}'
    - '\definecolor{links}{HTML}{661100}'
    - '\hypersetup{colorlinks,linkcolor=,urlcolor=links}'
title: Déploiement de JupyterHub/GPU avec Kubernetes
subtitle: "Méthodes cloud appliquées à la fourniture de ressources de calcul"
author: Rémi Cailletaud
date: 21 janvier 2021
---
# \faPlayCircle Intro

:::: {.columns}
::: {.column width="35%"}
![stack](stack.png){ width=150px }\
:::
::: {.column width="65%"}
## Objectifs:
* Environnement de développement/recherche à la demande: *notebooks*
* Déploiements rapides, reproductibles
* Contrôle des ressources

## Architecture:
* OpenStack
* Kubernetes
* JupyterHub
:::
::::


# \faCloud OpenStack

* Fournisseur de ressources : CPU, RAM, disque, GPU ...
* API ouverte
* Infrastructure programmable 

# \faDharmachakra Kubernetes: avertissement \faExclamationTriangle

![Une réputation méritée ?](learning-curve.png){ width=250px }

# \faDharmachakra Kubernetes: historique

* Deux projets Google dont Borg, en Java
* Réécriture en Go, version 1.0 en 2015
* Pilotage par Google, puis par la Cloud Native Computing Foundation (Linux Foundation) depuis août 2018

# \faDharmachakra Kubernetes

> *« We must treat the datacenter itself as one massive warehouse-scale computer [^1]. »*

[^1]: in *The Datacenter as a Computer: An Introduction to the Design of Warehouse-Scale Machines*, André B.L., Clidaras J., Hölzle U

# \faDharmachakra Kubernetes: objectifs

* L'abstraction des couches matérielles et système
* Le couplage faible des composants
* Un surcoût minimal
* Fonctionnement indifférent sur machines physiques et sur machine virtuelles

# \faDharmachakra Kubernetes: concepts

* Une API déclarative: on définit des contrats pour nos applications
* Des capacités d'autoréparation
* Une infrastructure immutable

# \faDharmachakra Kubernetes: architecture

:::: {.columns}
::: {.column width="50%"}
![Architecture](k8s-architecture.png){ width=200px }
:::
::: {.column width="50%"}
* Une séparation nette du plan de travail et du plan de contrôle
* Configuration par un point unique, via l'API
* Composants faiblement couplés : communication uniquement avec l'API
:::
::::

# \faDharmachakra Kubernetes: le plan de contrôle

:::: {.columns}
::: {.column width="45%"}
![Plan de contrôle](k8s-controlplane.png){ width=120px }
:::
::: {.column width="55%"}
* L'API au centre du système
* etcd pour le stockage clé/valeur
* Des boucles de contrôle: *kube-controller-manager*
* Un scheduler: *kube-scheduler*
* L'intégration à l'infrastructure sous-jacente: *cloud-controller-manager*. 
:::
::::

# \faDharmachakra Kubernetes: le plan de travail

:::: {.columns}
::: {.column width="35%"}
![Plan de travail](k8s-workplane.png){ width=120px }
:::
::: {.column width="65%"}
* Gestion des charges de travail (*pods*) : *kubelet*
* Gestion des règles de *forwarding* et de l'équilibrage de charge : *kube-proxy*
* Un *runtime* compatible *Container Runtime Interface* : Docker, containerd, rkt, CRI-O, Kata, Singularity-CRI..
:::
::::

# \faDharmachakra Kubernetes: les objets de base de l'API

* **Namespace**: conteneur logique
* **Pod**: unité de base
* **Deployment**, **StatefulSet**, **DaemonSet**: ordonnancement
* **Service**: réseau et répartition de charge
* **Job**, **CronJob**: tâches
* **PersistentVolume**, **PersistentVolumeClaim**: volumes persistants
* **ConfigMap**, **Secret**: configuration et secret
* **Ingress**: règles de *reverse proxy*

# \faDharmachakra Kubernetes: *manifest*
\small
```yaml
apiVersion: batch/v1
kind: Job
metadata:
  name: hello
spec:
  template:
    # This is the pod template
    spec:
      containers:
      - name: hello
        image: busybox
        command: ['sh', '-c', 'echo "Hello, Kubernetes!"']
      restartPolicy: OnFailure
    # The pod template ends here
```

# \faHubspot JupyterHub

* Serveur de *notebooks* (singleuser)
* Personnalisable: langage, interface, ...
* Flexible, scalable, portable
* Plusieurs *spawners* disponibles: *SystemdSpawner*, *SudoSpawner*, *DockerSpawner*, **KubeSpawner**...

L'utilisation d'images Docker facilite la création et la distribution d'environnements (transfert vers l'univers du HPC par ex.)

# \faHubspot JupyterHub: KubeSpawner

Avantages: isolation, contrôle des ressources, passage à l'échelle

![z2jh](z2jh.png)\

# \faTools Les outils: Packer

:::: {.columns}
::: {.column width=35%}
![Packer](packer-vertical-color.png){ width=150px }\
:::
::: {.column width=65%}
* Hashicorp, Open Source (MPL2)
* Construction d'image de VM
* Multi plateforme (VirtualBox, vSphere, Openstack, différents *cloud providers*)
* Utilisation possible d'un *SCM* (puppet, ansible, salt, chef)
:::
::::

# \faTools Les outils: Packer

\tiny
```json
{
    "builders": [
        {
            "type": "openstack",
            "image_name": "ubuntu-18.04-docker-x86_64",
            "source_image_name": "ubuntu-18.04-bionic-x86_64",
            "ssh_username": "ubuntu",
            "networks": [
                "59981445-9379-44f2-ad36-a4e91e174f96"
            ],
            "floating_ip_network": "b7658133-6810-4804-aa76-a0ab096092ee",
            "floating_ip": "7ac29e38-3332-4a21-b568-ab2d8fac6600",
            "security_groups": "default"
        }
    ],
    "provisioners": [
      {
        "type": "salt-masterless",
        "bootstrap_args": "-X -d -D stable 2019.2.0",
        "local_state_tree": "./salt",
        "custom_state": "docker",
        "salt_call_args": "pillar='{\"docker_version\": \"5:18.09.1~3-0~ubuntu-bionic\"}'"
      },
      {
        "type": "shell",
        "inline": ["sudo adduser ubuntu docker"]
      }
    ]
}
```
# \faTools Les outils: Terraform

:::: {.columns}
::: {.column width=35%}
![Terraform](terraform-vertical-color.png){ width=150px }\
:::
::: {.column width=65%}
* Hashicorp, Open Source (MPL2)
* *Infrastructure as Code*
* Multi plateforme (plusieurs centaines de fournisseurs)
* Gestion des dépendances
* Gestion et partage de l'état de l'infrastructure
:::
::::

# \faTools Les outils: Terraform

\small
```json
resource "openstack_compute_instance_v2" "instance" {
  name         = "nvidialic"
  image_name   = "ubuntu-18.04.4-bionic-x86_64"
  flavor_name  = "m1.medium"
  key_pair     = "cailletr-rsa"
  network {
    uuid = openstack_networking_network_v2.nvidialic_net.id
    fixed_ip_v4 = "192.168.0.10"
  }
}
```

# \faTools Les outils: Helm

:::: {.columns}
::: {.column width=35%}
![Helm](helm-stacked-color.png){ width=150px }\
:::
::: {.column width=65%}
* *Package manager* pour Kubernetes (charts)
* Système de template pour K8S
* Gestion des dépôts
* De très nombreux logiciels empaquetés
:::
::::

# \faSitemap La pratique: vue d'ensemble

![Architecture](ust4hpc-general.png)

# \faSitemap La pratique: les outils

## [terraform-openstack-rke](https://github.com/remche/terraform-openstack-rke)
* *a Terraform module to deploy Kubernetes with RKE on OpenStack*
* Création des routeurs, réseaux, groupes de sécurité, machines virtuelles, IP flottantes et déploiement de Kubernetes
* utilisation de [terraform-provider-openstack](https://github.com/terraform-provider-openstack/terraform-provider-openstack) et [terraform-provider-rke](https://github.com/rancher/terraform-provider-rke)

## [Zero To JupyterHub with Kubernetes (z2jh)](https://github.com/jupyterhub/zero-to-jupyterhub-k8s)
* *a Helm chart for JupyterHub*
* Déploiement des différents composants de JupyterHub à l'aide d'un chart Helm

# \faGavel Debrief

## Souplesse
* [Dask](https://github.com/dask/helm-chart), [ShinyProxy](https://github.com/remche/zero-to-shinyproxy), [KubeFlow](https://www.kubeflow.org/), [Hkube](https://github.com/kube-HPC/hkube)
* Utilisateurs administrateurs

## Elasticité
* Cohabitation des charges de travail
* Débordement sur des services commerciaux...
* Mais pas de véritable ordonnancement

## Edge computing
* Normalisation des infrastructures
* Amener les ressources de calcul près de la donnée

\faExclamationTriangle Mais attention aux ressources humaines 

# \faCreativeCommons\ \faCreativeCommonsBy\ \faCreativeCommonsSa

Merci de votre attention !

## UST4HPC
* [Présentation JupyterHub/GPU avec K8S](https://gricad-gitlab.univ-grenoble-alpes.fr/ust4hpc/talk)
* [TP gpu-hub](https://gricad-gitlab.univ-grenoble-alpes.fr/ust4hpc/gpu-hub)

## Autres resources
* [JRES 2019: Article Kubernetes](https://conf-ng.jres.org/2019/document_revision_4761.html?download)
* [ANF ADA: Kubernetes TP](https://plmlab.math.cnrs.fr/resinfo/ANF/2019/ADA/kubernetes/vapormap)

\
Made with [Pandoc](https://pandoc.org), [Metropolis Beamer Theme](https://github.com/matze/mtheme) and [FontAwesome](https://fontawesome.com/)

