# L'infrastructure - généralités
- présentation très rapide d'openstack (éviter la redite avec Jérome). Insister sur les points pertinents (vGPU, scheduling)
- présentation de Kubernetes

# Les outils utilisés
- présentation de Packer, Terraform et Helm

# Dans la pratique
- présentaton générale de l'infra déployée (avec les outils: terraform-openstack-rke, z2jh)
- déploiement, avec un description préalable des fichiers de configuration


-> chiffres surcout 5% par nœud, 1% pour l’ensemble du cluster;


TP

* présentation terraform-openstack-rke. parler de traefik 
* images de VM (drivers GRID, docker, docker, nvidia-docker2)
* fonctionnalité d'ajout DNS automatique
* plugin k8s-device-plugin

kubectl get nodes -o=custom-columns=NAME:.metadata.name,GPUs:.status.capacity.'nvidia\.com/gpu'

* image FIDLE (nvidia + jupyterhub + dépendances)

import tensorflow as tf
tf.test.is_gpu_available(cuda_only=True) # True/False

* http://huit.re/ust4hpc-gpu-hub
